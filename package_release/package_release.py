import os
import sys
import logging

class Package_Release():
    def __init__(self,
                 package_name: str,
                 version: str,
                 package_index: str,
                 repository_link: str) -> None:
        
        self.package_name = package_name
        self.version = version
        self.package_index = package_index
        self.repository_link = repository_link       

    def __len__(self):
        pass

    def runner(self):
        return self.package_name + "_" + self.version + "_" + self.package_index + "_" + self.repository_link
 