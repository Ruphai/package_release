from package_release import package_release

package_releaser = package_release.Package_Release(package_name = "Package Releaser",
                                                   version = "0.0.1",
                                                   package_index = "TestPyPI",
                                                   repository_link = "https://gitlab.com/Ruphai/package_release"
)

package_releaser.runner()

